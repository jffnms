<?
/* This file is part of JFFNMS
 * Copyright (C) <2006> Javier Szyszlican <javier@szysz.com>
 * This program is licensed under the GNU GPL, full terms in the LICENSE file
 */
function poller_verify_storage_index ($options) {

    $hrStorageDescr_oid = ".1.3.6.1.2.1.25.2.3.1.3";

    $index_actual = -1;

    if (!is_array($GLOBALS["verify_storage_index_data"]) && !empty($options["ro_community"])) {

        $aux = snmp_walk($options["host_ip"],$options["ro_community"],
	    $hrStorageDescr_oid, INCLUDE_OID_1);

	include_once(jffnms_shared("storage"));
	
	if (is_array($aux))
	while (list($k, $v) = each ($aux))	// Parse each description to match what we do in discovery
	    $GLOBALS["verify_storage_index_data"][$k]=current(storage_interface_parse_description ($v));

	unset ($aux);
    }

    if (is_array($GLOBALS["verify_storage_index_data"])) {
	$storage_name = $options["interface"];

	if (($index_actual = array_search($storage_name, $GLOBALS["verify_storage_index_data"]))===false)
	    $index_actual = 90000+$options["interface_id"];	// if its not found in the host, change its id out of the picture for removal
    }

    return $index_actual;		    
}
?>
